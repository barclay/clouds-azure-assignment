# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import os
from azure.storage.blob import BlobServiceClient

connection_string = "DefaultEndpointsProtocol=https;AccountName=breadydurablestorage1;AccountKey=ZGQ7PHAIERHrtqG1KKl5dtuSEMCGPHjh4QhseQ0SFZiMJ0Ons6yqdn+K6b+5squgbo2jJwOMFxY++ASto+CHEg==;EndpointSuffix=core.windows.net"

def main(name: str) -> list:
    
    totallines = []

    bsc = BlobServiceClient.from_connection_string(connection_string)
    container_client = bsc.get_container_client(container="filesblob") 

    blob_list = container_client.list_blobs()
    for blob in blob_list:
        blob_content = container_client.download_blob(blob.name).readall()
        lines = blob_content.splitlines()
        for i in range(len(lines)):
            totallines.append((i+1,str(lines[i])))
    return totallines
