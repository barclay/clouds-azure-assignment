# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df

def flattenResult(array2d):
    flat_list = []
    for row in array2d:
        flat_list += row
    return flat_list


def orchestrator_function(context: df.DurableOrchestrationContext):

    # mockData = [
    #     (1, "Tokyo is a city in a country and is in Japan"                          ),
    #     (2, "Seattle Seahawks are a football team in the NFL"   ),
    #     (3, "London is a city with football teams"              )
    # ]

    input_data = yield context.call_activity("GetInputDataFn", None)

    mapTask = [context.call_activity("Mapper", m) for m in input_data]
    mappedBatch = yield context.task_all(mapTask)
    mapped = flattenResult(mappedBatch)

    shuffled = yield context.call_activity('Shuffler', mapped)
    
    reduceTask = [context.call_activity("Reducer", s) for s in shuffled]
    reduced = yield context.task_all(reduceTask)
    return reduced
    

main = df.Orchestrator.create(orchestrator_function)