# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(payload: list) -> list:
    res = {}
    for (word,count) in payload:
        if(word in res ):
            res[word].append(count)
        else:
            res[word] = [count]
    return list(res.items())
