import math
import numpy as np
from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/")
def hello_world():
    lower = float(request.args.get("lower"))
    upper = float(request.args.get("upper"))

    result_str = ""

    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        area = integrate(0, math.pi, N, abssin)
        # print(f"N = {N:7}: {area}")
        result_str += f"N = {N:7}: {area} <br>"


    return f"<p>Lower: {lower}</p> \
        <p>Upper: {upper} </p> \
        <p> Results: <br> {result_str} </p>"

def abssin(x):
    return np.abs(math.sin(x))


def integrate(lower, upper, N, f):
    step = (upper-lower) / N
    total_area = 0
    for i in np.arange(lower, upper, step):
        x_i = f(i)
        rect_area = step * x_i
        total_area += rect_area
    return total_area
