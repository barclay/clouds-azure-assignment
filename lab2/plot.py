import pandas as pd
from matplotlib import pyplot as plt
plt.rcParams["figure.figsize"] = [7.00, 3.50]
plt.rcParams["figure.autolayout"] = True

run0 = pd.read_csv("run0_1_stats_history.csv")
run0["diff"] = run0["Requests/s"] - run0["Failures/s"]
run0 = run0.drop("Requests/s", axis=1)
run0 = run0.drop("Failures/s", axis=1)

run1 = pd.read_csv("run1_stats_history.csv")
run1["diff"] = run1["Requests/s"] - run1["Failures/s"]
run1 = run1.drop("Requests/s", axis=1)
run1 = run1.drop("Failures/s", axis=1)

run2 = pd.read_csv("run2_1_stats_history.csv")
run2["diff"] = run2["Requests/s"] - run2["Failures/s"]
run2 = run2.drop("Requests/s", axis=1)
run2 = run2.drop("Failures/s", axis=1)

run3 = pd.read_csv("run3_stats_history.csv")
run3["diff"] = run3["Requests/s"] - run3["Failures/s"]
run3 = run3.drop("Requests/s", axis=1)
run3 = run3.drop("Failures/s", axis=1)

plt.plot(run0, label='local deployment')
plt.plot(run1, label="scaleset")
plt.plot(run2, label="webapp")
plt.plot(run3, label="function")

plt.ylabel("Successfull Requests/s")
plt.xlabel("Time")
plt.legend()

plt.show()