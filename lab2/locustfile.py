import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def hello_world(self):
        self.client.get("http://localhost:5000/?lower=0&upper=3.141")
        # self.client.get("http://104.211.24.215/?lower=0&upper=3.141")
        # self.client.get("https://integral-webapp.azurewebsites.net/?lower=0&upper=3.141")
        # self.client.get("https://breadyintegralfunction.azurewebsites.net/api/integralfunction?lower=0&upper=3.141")

