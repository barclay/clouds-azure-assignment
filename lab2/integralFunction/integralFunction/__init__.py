import logging

import azure.functions as func

import numpy as np
import math



def abssin(x):
    return np.abs(math.sin(x))


def integrate(lower, upper, N, f):
    step = (upper-lower) / N
    total_area = 0
    for i in np.arange(lower, upper, step):
        x_i = f(i)
        rect_area = step * x_i
        total_area += rect_area
    return total_area

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.params.get('lower')
    upper = req.params.get('upper')

    if not lower or not upper:
        return func.HttpResponse(
             "Invalid Request",
             status_code=400
        )
    result_str = ""

    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        area = integrate(0, math.pi, N, abssin)
        # print(f"N = {N:7}: {area}")
        result_str += f"N = {N:7}: {area} \n"


    return func.HttpResponse(f"Lower: {lower} \
        Upper: {upper} \n\
        Results: \n\
        {result_str}", status_code=200)





